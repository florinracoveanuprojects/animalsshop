<?php


function printMenu(){
    global $mysqlConnect;
	$roStatus = '';
	$enStatus = '';
	session_start();
	if(!isset($_SESSION['language'])){
		$_SESSION['language'] = 'ro';
	}
	if(isset($_POST["submitCheckBox"])){
		$_SESSION['language'] = $_POST['language'];
	}
	if($_SESSION['language'] == 'ro'){
		$roStatus = 'checked';
		$enStatus = 'unchecked';
	}else{
		$enStatus = 'checked';
		$roStatus = 'unchecked';
	}
	$result = mysqli_query($mysqlConnect, "SELECT * from menu WHERE language='".$_SESSION['language']."'");
	$menuItems = $result->fetch_all(MYSQLI_ASSOC);
	?>
    <table width="95%" align="center" bgcolor="white">
        <tr align="center">
            <td>
                <ul>
                    <?php foreach ($menuItems as $menuItem): ?>
                        <li><a href="<?php echo $menuItem['url']; ?>"><?php echo $menuItem['title']; ?></a></li>
                    <?php endforeach; ?>
					<form method="post" style="float:right">
						<input type="radio" <?php echo $roStatus; ?> name="language" value="ro">RO
						<input type="radio" <?php echo $enStatus; ?> name="language" value="en"> EN
						<button type="submit" class="button" name="submitCheckBox" style="float:right">Schimba</button>
					</form>
				</ul>
            </td>
        </tr>
    </table>
<?php
}


function printProduct($productItem, $key){
	
	$priceFinal = $productItem['price'] - $productItem['discount']/100*$productItem['price'];
	?>
	<td width="25%">
		<img src="images/<?php echo $productItem['image']; ?>" width="100">
		<p><?php echo $productItem['name']; ?></p>
		<h4 class="red"><p style="color:#c7ccd6"><del><?php echo $productItem['price']; ?> lei</del> (-<?php echo $productItem['discount']; ?> %)</p><?php echo $priceFinal; ?> lei</h4>
		<a href="<?php echo $productItem['link']; ?>?key=<?php echo $key;?>" class="button">Alege produsul</a>
	</td>
	<?php
}

function printFooter($footerItems){
	?>
	<table width="95%" align="center" bgcolor="#cccccc">
	<tr>
		<?php 
		foreach($footerItems as $footerItem){
			?><td><?php
			?><h3><?php echo $footerItem['title']; ?></h3>
			<a href="<?php $footerItem['url1'];?>" class="footerText"><?php echo $footerItem['description1']; ?></a><br />
			<a href="<?php $footerItem['url2'];?>" class="footerText"><?php echo $footerItem['description2']; ?></a><br />
			<a href="<?php $footerItem['url3'];?>" class="footerText"><?php echo $footerItem['description3']; ?></a><br />
			</td>
			<?php
		}
		?>	
	</tr>
</table>
	<?php
}

function printSidebar($sidebarItems){
	foreach($sidebarItems as $sidebarItem){
		?><a href="<?php echo $sidebarItem['url']; ?>" style="text-decoration:none; color:#999999"><?php echo $sidebarItem['title']; ?></a><br /><br /><?php
	}
}
