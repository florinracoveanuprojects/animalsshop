<html>
	<head>
		<title>Happy animals</title>
		<link rel="stylesheet" href="style.css">
	</head>
	<body>
		<table width="55%" align="center" bgcolor="#f2f2f2">
			<tr>
				<td>
					<!--Header, Product, Sidebar-->
					<!--Header-->
					<?php include"parts/header.php"; ?>
					<!--Products-->
					<table width="95%" align="center" cellspacing="30" bgcolor="white">
						<tr align="center" valign="top">
							<td width="75%">
								<!--Product-->
								<table width="100%">
									<tr align="center">
										<?php 
										$result = mysqli_query($mysqlConnect,"SELECT * FROM products");
										$productItems = $result->fetch_all(MYSQLI_ASSOC);
										foreach($productItems as $key => $product){
											if(($key%3 == 0) && ($key != 0)){
												?></tr><tr align="center"><?php
											}
											printProduct($product, $key);
										}
										?>
									</tr>
								</table>
							</td>
							<!--Sidebar-->
							<?php include"parts/sidebar.php"; ?>
						</tr>
					</table>
					<!--Footer-->
					<?php include"parts/footer.php"; ?>
				</td>
			</tr>
		</table>
	</body>
</html>