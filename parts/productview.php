<?php
$result = mysqli_query($mysqlConnect, "SELECT * from products INNER JOIN bag ON products.id = bag.product_id");
$products = $result->fetch_all(MYSQLI_ASSOC);
$numberOfBoughtProducts = mysqli_num_rows($result);
$priceTotal = 0;
?>

<?php foreach($products as $product){
	$priceFinal = $product['price'] - $product['discount']/100*$product['price'];
	$priceTotal += $priceFinal;
	?>
	<tr align="center">
		<td width="15%"><img src="images/<?php echo $product['image']; ?>" width="130"></td>
		<td width="55%" style="text-align:left"><p><?php echo $product['description'];?></p></td>
		<td width="20%"><h4><span class="red"><?php echo $priceFinal;?></span> lei</h4></td>
		<td width="10%"><input type="image" src="images/x.png" alt="Submit" width="38" height="38"></td>
	</tr>
<?php
}
if($numberOfBoughtProducts != 0){
	?>
	</table>
	<table width="95%" align="center" bgcolor="white">
		<tr align="center">
			<td width="70%" align="right"></td>
			<td width="30%">
				<table border="1" width="100%">
					<tr align="center">
						<td><p><b>Pret total: <span class="red"><?php echo $priceTotal; ?> </span>lei</b></p></td>
					</tr>
				</table>
			</td>		
		</tr>
	<?php
}

?>