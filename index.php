<html>
	<head>
		<title>Happy animals</title>
		<link rel="stylesheet" href="style.css">
	</head>
	<body>
		<table width="55%" align="center" bgcolor="#f2f2f2">
			<tr>
				<td>
					<!--Header, Product-->
					<!--Header-->
					<?php include"parts/header.php"; ?>
					<!--Products-->
					<table width="95%" align="center" cellspacing="30" bgcolor="white">
						<tr align="center" valign="top">
							<?php include "parts/product.php"; ?>				
						</tr>
						<br/>
					</table>
					<!--Footer-->
					<?php include"parts/footer.php"; ?>
				</td>
			</tr>
		</table>
	</body>
</html>