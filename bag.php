<html>
	<head>
		<title>Happy animals</title>
		<link rel="stylesheet" href="style.css">
	</head>
	<body>
		<table width="55%" align="center" bgcolor="#f2f2f2">
			<tr>
				<td>
					<!--Header, Product, Finalize-->
					<!--Header-->
					<?php include"parts/header.php"; ?>
					<!--Products-->
					<table width="95%" align="center" border="1" bgcolor="white">
						<tr>
							<td><h4>Imagine</h4></td>
							<td><h4>Descriere</h4></td>
							<td><h4>Pret</h4></td>
							<td><h4>Elimina</h4></td>
						</tr>
						<?php include"parts/productview.php"; ?>
					</table>
					<!--Finalize-->
					<table width="95%" bgcolor="white" align="center">
						<tr>
							<td>
								<br /><a href="" class="button" style="float:right">Finalizeaza comanda</a>
							</td>
						</tr>
					</table>
					<!--Footer-->
					<?php include"parts/footer.php"; ?>
				</td>
			</tr>
		</table>
	</body>
</html>