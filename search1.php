<link rel="stylesheet" href="style.css">
<form name="form" action="search1.php" method="get">
  <input type="text" name="searchInput">
  <button type="submit">Search</button>
</form>
<?php

include "config.php";
include "functions.php";
session_start();

if(!isset($_SESSION['searchInput'])){
	$_SESSION['searchInput'] = $_GET['searchInput'];
}
$numberAparitions = 0;
$numberProductsPerPage = 2;

if(!isset($_GET['searchInput'])){
	 $_GET['searchInput'] = 0;
}

if((isset($numberAparitions)) && (isset($printedProducts))){
	$remainedPages = $numberAparitions - $printedProducts;
}else{
	$remainedPages = $numberProductsPerPage;
}

if(!isset($printedProducts)){
	$printedProducts = 0;
}

foreach($productItems as $productItem){
	if((strlen(stristr($productItem['category'], $_SESSION['searchInput'])) > 0) || (strlen(stristr($productItem['name'], $_SESSION['searchInput'])) > 0)){
		$numberAparitions++;
	}
}

$numberOfPages = ceil($numberAparitions/$numberProductsPerPage);

for($key = $printedProducts; $key < $remainedPages; $key++){
	if((strlen(stristr($productItems[$key]['category'], $_GET['searchInput'])) > 0) || (strlen(stristr($productItems[$key]['name'], $_GET['searchInput'])) > 0)){
		
		printProduct($productItems[$key], $key);
		$printedProducts++;
		echo "<br />";
	}
}
for($i = 1; $i <= $numberOfPages; $i++){?>
	<a href="search1.php?page=<?php echo $i;?>"><?php echo $i; ?></a><?php
}



?>