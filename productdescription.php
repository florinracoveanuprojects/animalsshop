<html>
	<head>
		<title>Happy animals</title>
		<link rel="stylesheet" href="style.css">
	</head>
	<body>
		<!--Header, Pictures, Similars, Description-->
		<!--Header-->
		<table width="55%" align="center" bgcolor="#f2f2f2">
			<tr>
				<td>
					<?php include"parts/header.php"; ?>
					<?php 
					$result = mysqli_query($mysqlConnect, "SELECT * from products where id='".$_GET['key']."'");
					$products = $result->fetch_all(MYSQLI_ASSOC);
					//var_dump($products);
					$product=$products[0];
					$category = $product['category'];
					$priceFinal = $product['price'] - $product['discount']/100*$product['price'];
					?>
					<table width="95%" align="center" bgcolor="white">
						<!--Pictures-->
						<tr>
							<td>
								<br><h3><?php echo $product['name']; ?></h3><hr>
							</td>
						</tr>
						<tr>
							<td align="center" width="80%">
								<img src="images/<?php echo $product['image']; ?>" width="200"><br /><br /><br />
								<img src="images/<?php echo $product['image']; ?>" width="90">
								<img src="images/<?php echo $product['image']; ?>" width="90">
								<img src="images/<?php echo $product['image']; ?>" width="90">
							</td>
							<td>
								<table width="100%" bgcolor="#cceeff" style="border-radius:10px; padding: 10px;">
									<tr>
										<td>
											
											<p>Retur in <b>30</b> de zile</p>
											<p>Deschidere colet la livrare</p>
											<hr>
											<h4 class="red"><p style="color:#c7ccd6"><del><?php echo $product['price']; ?> lei</del> (-<?php echo $product['discount']; ?> %)</p><?php echo $priceFinal; ?> lei</h4>
											<form method="post">
												<div align="center">
													<input type="submit" name="btnSubmit" class="button" value="Adauga in cos">
												</div>
												<?php
												if (isset($_POST["btnSubmit"])){
													$sql = "INSERT INTO bag (product_id) VALUES ('".$_GET['key']."')";
													if ($mysqlConnect->query($sql) === TRUE) {
													//echo "New record created successfully";
													} else {
													// echo "Error: " . $sql . "<br>" . $conn->error;
													}
												}
											?>
											</form>
										</td>
									</tr>
								</table>
								<br />
								<table width="100%" bgcolor="#cceeff" style="border-radius:10px; padding: 10px;">
									<tr>
										<td>
											<h4>Aplica un cod de voucher</h4>
											<hr><br />
											<div align="center">
												<input type="text">
												<a href="productdescription.php" class="button">Aplica</a>
											</div>	
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
					<!--Similars-->
					<table width="95%" align="center" bgcolor="white">		
						<h2 style="margin-left:2.5%">Produse similare</h2>
						<tr align="center">
							<?php 
								$result = mysqli_query($mysqlConnect, "SELECT * from products WHERE id != '".$_GET['key']."' AND category = '".$category."' ORDER BY RAND() Limit 4");
								$productItems = $result->fetch_all(MYSQLI_ASSOC);
								$numberOfSimilarElements = mysqli_num_rows($result);
								//print_r($productItems);
								if($numberOfSimilarElements == 4){
									for($i = 0; $i < 4; $i++){
										$key = $productItems[$i]['id'];
										printProduct($productItems[$i],$key);
								}
								}else{
									?><td align="center" style="background-color:white"><?php echo "Nu sunt produse similare";?></td><?php
								}
							?>
						</tr>
					</table>	
					<!--Description-->
					<table width="95%" align="center" bgcolor="white">
						<h2 style="margin-left:2.5%">Descriere</h2>		
						<tr>
							<td align="center">
								<br /><p><?php echo $product['description']; ?></p><br />
							</td>
						</tr>
					</table>
					<br /><br />
					<!--Footer-->
					<?php include"parts/footer.php"; ?>
				</td>
			</tr>	
		</table>
	</body>

</html>