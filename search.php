<link rel="stylesheet" href="style.css">
<form name="form" action="search.php" method="get">
  <input type="text" name="searchInput">
  <button type="submit">Search</button>
</form>
<?php
include "config.php";
include "functions.php";
session_start();
$numberItemPerPage = 2;
$counter = 0;

if(isset($_GET['page'])){
	$pages = $_GET['page'];
}else{
	$pages = 1;
}

if($pages > 1){
	$startPage = ($pages*$numberItemPerPage)-$pages;
}else{
	$startPage = 0;
}
if(isset($_GET['searchInput'])){
	$_SESSION['searchInput'] = $_GET['searchInput'];
	for($i = $startPage; $i <= $numberItemPerPage; $i++){
		$productItem = $productItems[$i];
		if((strlen(stristr($productItem['category'], $_SESSION['searchInput']))>0) || (strlen(stristr($productItem['name'], $_SESSION['searchInput']))>0)){
			printProduct($productItem, $i);
			$counter++;
			echo "<br />";
		}
	}
}


$pages = $counter;
?>
<?php
for($i = 1; $i <= $pages; $i++){?>
	<a href="?page=<?php echo $i; ?>&numberItemPerPage=<?php echo $numberItemPerPage; ?>"><?php echo $i; ?></a>
	<?php
}
?>

